$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
    var up = "https://png.icons8.com/collapse-arrow/win10/50/000000";
    var down = "https://png.icons8.com/expand-arrow/win10/50/000000";
    var hidden = false;
    $(".chat-text").bind("keyup", function(e) {
        console.log(e.keyCode);
        if (e.keyCode == 13 ){
            var str = $("textarea").val();
            $("textarea").val('');
            $(".msg-insert").append("<div class=\"msg-send\">" + str + "</div>");
            $(".msg-insert").append("<div class=\"msg-receive\">" + str + "!</div>");
        }
    })

    $("img:last-child").bind("click", function () {
        $(".chat-body").toggle("slow");
        if(!hidden) {
            hidden = true;
            $("img").attr('src', up);
        } else {
            hidden = false;
            $("img").attr('src', down);
        }
    })

    $('.my-select').select2();
    // Calculator
    var print = document.getElementById('print');
    var erase = false;

    var go = function(x) {
      if (x === 'ac') {
        /* implemetnasi clear all */
        print.value = "";
      } else if (x === 'eval') {
          print.value = Math.round(evil(print.value) * 10000) / 10000;
          erase = true;
      } else {
        print.value += x;
      }
    };

    function evil(fn) {
      return new Function('return ' + fn)();
    }
    // END
});
