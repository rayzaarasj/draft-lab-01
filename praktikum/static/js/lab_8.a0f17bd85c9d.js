window.fbAsyncInit = function() {
    FB.init({
        appId   : '1490314854338471',
        cookie  : true,
        xfbml   : true,
        version : 'v2.11'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.parentNode.insertBefore(js, fjs);
} (document, 'script', 'facebook-jssdk'));

function facebookLogin() {
    FB.login(function(response){
        console.log(response);
    }, {scope:'public_profile'})
}
