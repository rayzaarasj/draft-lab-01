from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

response = {'author': 'Zardfew'}
csui_helper = CSUIhelper()


def index(request):
    page = request.GET.get('page', '1')
    prev = int(page) - 1 if int(page) - 1 < 0 else 1
    next = int(page) + 1
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list(page)
    friend_list = Friend.objects.all()

    response['mahasiswa_list'] = mahasiswa_list
    response['friend_list'] = friend_list
    response['prev'] = prev
    response['next'] = next

    html = 'lab_7/lab_7.html'
    return render(request, html, response)


def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)


def friend_list_json(request):  # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    if len(friends) > 0:
        return JsonResponse(
            {"results": friends},
            content_type='application/json'
        )


@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())


def delete_friend(request, friend_npm):
    Friend.objects.filter(npm=friend_npm).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list')


@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
