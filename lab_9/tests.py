from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .api_enterkomputer import get_drones, get_soundcard, get_optical
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_index_redirected_when_logged_in(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')
        self.client.post('/lab-9/custom_auth/logout/')

    def test_lab_9_index_redirected_when_not_logged_in(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_add_delete_and_reset_favorite_item(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        #add drone
        response = self.client.post('/lab-9/add_session_item/drones/'+get_drones().json()[0]["id"]+'/')
        response = self.client.post('/lab-9/add_session_item/drones/'+get_drones().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drones favorite", html_response)

        #delete drone
        response = self.client.post('/lab-9/del_session_item/drones/'+get_drones().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus item drones dari favorite", html_response)

        #reset drone
        response = self.client.post('/lab-9/clear_session_item/drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus session : favorite drones", html_response)

        #add soundcard
        response = self.client.post('/lab-9/add_session_item/soundcards/'+get_soundcard().json()[0]["id"]+'/')
        response = self.client.post('/lab-9/add_session_item/soundcards/'+get_soundcard().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah soundcards favorite", html_response)

        #delete soundcard
        response = self.client.post('/lab-9/del_session_item/soundcards/'+get_soundcard().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus item soundcards dari favorite", html_response)

        #reset soundcards
        response = self.client.post('/lab-9/clear_session_item/soundcards/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus session : favorite soundcards", html_response)

        #add optical
        response = self.client.post('/lab-9/add_session_item/opticals/'+get_optical().json()[0]["id"]+'/')
        response = self.client.post('/lab-9/add_session_item/opticals/'+get_optical().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah opticals favorite", html_response)

        #delete optical
        response = self.client.post('/lab-9/del_session_item/opticals/'+get_optical().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus item opticals dari favorite", html_response)

        #reset opticals
        response = self.client.post('/lab-9/clear_session_item/opticals/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus session : favorite opticals", html_response)

    def test_logout(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)

    def test_custom_auth_wrong_username_password(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': 'a', 'password': 'a'})
        self.assertEqual(response.status_code, 302)
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertIn("Username atau password salah", html_response)

    def test_cookie(self):
        #not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        #login using HTTP GET method
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        #login failed, invalid pass and uname
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'u', 'password': 'p'})
        html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Username atau Password Salah", html_response)

        #try to set manual cookies
        self.client.cookies.load({"user_login": "u", "user_password": "p"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Kamu tidak punya akses :P ", html_response)

        #login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

        #logout
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Anda berhasil logout. Cookies direset", html_response)

