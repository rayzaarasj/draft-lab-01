from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
    response["author"] = "Rayza"
    html = "lab_6/lab_6.html"
    return render(request, html, response)
